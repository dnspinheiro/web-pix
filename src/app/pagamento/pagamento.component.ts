import { HttpClient } from '@angular/common/http';
import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataFormComponent } from '../shared/data-form.component';
import { HttpService } from '../shared/service/http.service';

@Component({
  selector: 'app-Pagamento',
  templateUrl: './Pagamento.component.html',
  styleUrls: ['./Pagamento.component.css']
})
// extend component com funções basicas de cadastro em formulario
export class PagamentoComponent extends DataFormComponent implements OnInit {

  service: any;

  pagamentos: any;

  constructor(protected formBuilder: FormBuilder,
    protected http: HttpClient,
    protected injector: Injector) {
    super(injector, 'pagamentos');
    // Injeção para serviço http passando endpoint de api
    this.service = new HttpService(injector, 'pagamentos');
    this.listPagamentos();
  }

  ngOnInit() {
    // chama carregamento dos pagamentos construindo o formulario independente do resultado da listagem
    this.listPagamentos();
    this.makeForm();
  }

  makeForm() {
    this.formulario = this.formBuilder.group({
      id: [null],
      nome: [null, Validators.required],
      // regex para permitir apenas numeros e quantidade de cpf
      cpf: [null, [Validators.required, Validators.pattern('^[0-9]{3}[0-9]{3}[0-9]{3}[0-9]{2}')]],
      // regex para permitir mascara cpf
      // cpf: [null, [Validators.required, Validators.pattern('^[0-9]{3}.?[0-9]{3}.?[0-9]{3}-?[0-9]{2}')]],
      // contato: this.formBuilder.group({
      chave: [null, Validators.required],
      // email: [null, [Validators.required, Validators.email]],
      // telefone: [null, [Validators.required, Validators.pattern('^[0-9]{1,2}\\s[0-9]{4,5}[-][0-9]{4,5}')]],
      valor: [null, [Validators.required, Validators.pattern('^[0-9]*$')]],
      instituicao: [null, Validators.required],
      descricao: [null, Validators.required],
      data: [new Date(), Validators.required],
      // files: [null]
      // })
    });
  }

  enviar() {
    super.onSubmit().then(retorno => {
      this.makeForm();
      this.listPagamentos();
    });

  }

  delete(id) {
    super.onDelete(id);
    this.listPagamentos();
  }

  async listPagamentos() {
    // utilização do super para carregar recurso e aplica logica porcentagem
    // antes de apresentar em tela
    super.list().then(resources => {
      this.pagamentos = resources;
      this.porcentagemPorPagamento();
      this.porcentagemPorDia();
    }).catch(error => {
      console.log('error', error);
    });

  }
  // calcula porcentagem baseado no valor de dia do pagamento
  porcentagemPorDia() {
    // throw new Error('Method not implemented.');
  }

  // calcula porcentagem baseado no valor de cada pagamento
  porcentagemPorPagamento() {
    var totalMesAtual = this.pagamentos.reduce((total, element) => {
      if ((new Date(element.data).getMonth() + 1) == (new Date().getMonth() + 1)) {
        return total + Number(element.valor)
      } else {
        return Number(total)
      }
    }, 0);

    this.pagamentos.map(element => {
      return {
        ...element,
        porcentagem: element.porcentagem = ((Number(element.valor) * 100) / (totalMesAtual || 1)).toFixed(2)
      }
    })
  }

}
